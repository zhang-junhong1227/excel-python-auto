时隔一年，对比Excel系列的又一本新书发布，本来这本书应该早就出现在大家面前了，最近一年工作有些忙，所以就一直拖到了现在。新书就是下面这本《对比Excel，轻松学习Python报表自动化》。

![输入图片说明](https://images.gitee.com/uploads/images/2021/1017/225223_4364485f_9306977.png "1.png")

# 为啥写

还是聊聊为什么写这本书，写这本书的原因主要有两个：

第一个原因是这本书其实是第一本书《对比Excel，轻松学习Python数据分析》的补充，第一本书是围绕数据分析的全流程讲解每个环节中对应的Python代码如何实现。而**这本书聚焦报表自动化**，作为一名分析师，日常工作中少不了要做报表，尤其刚入行的分析师，会有大量的时间是在做报表，这本书就围绕怎么样利用Python将报表自动化，目的是**希望大家从报表中解放出来，能够有更多的时间投入到分析中。**

第二个原因是自从第一本书火了以后，这些年越来越多Excel + Python主题的书籍出现，我大概数了下，新书应该不止10本。我不知道这些新书的作者有没有受到小黄书（第一本书的封皮是黄色）的感染，或者是刚好大家想到一块去了，我不太确认，有的作者连书名字都和我想到一块去了，真是有缘人呐。很开心能够有这么多人喜欢并赞成这种写作方式，但是吧，我把这些新书都翻了翻以后，我决定写这本书了，**一本真正站在数据分析师角度的报表自动化书籍，而不是其他。**

# 写了啥
接下来聊聊这本书都写了啥，这本书的主题就是数据分析师角度的报表自动化，真正做过数据分析师的人应该知道，一份报表大体其实就两部分：**数据处理+格式设置**，数据处理就是对数据进行缺失值、重复值、异常值、还有加减乘除等处理，格式设置主要就是字体设置、单元格设置、条件格式这些。关于数据处理第一本书《对比Excel，轻松学习Python数据分析》讲的差不多了，这本书更多讲述格式设置和函数运算等内容。

整本书的框架其实都是围绕下面这张Excel菜单栏展开的：

![输入图片说明](https://images.gitee.com/uploads/images/2021/1017/225241_874b9f44_9306977.png "2.png")

依次为字体设置、对齐方式设置、数字格式设置、条件格式设置、单元格设置、函数运算、审阅和视图设置、图表可视化，除了Excel菜单栏中的内容以外还有对Excel文件进行批量操作、自动发送邮件、报表自动化实战等内容。

全书的思维导图框架如下：
![输入图片说明](https://images.gitee.com/uploads/images/2021/1017/225255_d4b10605_9306977.png "3.png")
